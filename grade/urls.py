# grade/urls.py
from django.urls import path
from .views import soldier_list, homepage, shift_list, shift_view, session_update

urlpatterns = [
    path('', homepage, name='home'),
    path('session_update/', session_update, name='session_update'),
    path('soldier/', soldier_list, name='soldier_list'),
    path('escala/', shift_list, name='shift_list'),
    path('escala/<int:arg>/', shift_view, name='shift_view'),
    #path('soldier/<str:my_arg>/', soldier_list, name='soldier_list'),
]