# pages/models.py
from datetime import time
from django.db import models
from django.db.models import Model
from django.core.validators import RegexValidator, MinLengthValidator
from django.contrib.auth.models import User

NIM_REGEX = RegexValidator(r'^[0-9]*$', "Introduza os 8 dígitos do NIM: '12345678'.")


class Regiment(Model):
    name = models.CharField(
        max_length = 100
        )
    short_name = models.CharField(
        max_length = 100
        )

    class Meta:
        verbose_name_plural = "UU/EE/OO"
        verbose_name = "U/E/O"
    
    def __str__(self):
        return self.short_name[:100]


class Holiday(Model):
    name = models.CharField(
        max_length = 100
        )
    date = models.DateField(
        help_text = "Introduza a data no seguinte formato, por favor: <em>AAAA-MM-DD</em>.",
        unique = True
        )
    #regiment = models.ForeignKey(
    #    Regiment,
    #    help_text = "Selecionar regimento, se se tratar de um feriado municipal.",
    #    on_delete=models.CASCADE,
    #    blank=True,
    #    null=True
    #    )

    class Meta:
        verbose_name_plural = "feriados"
        verbose_name = "feriado"
    
    def __str__(self):
        return self.name[:100]


class Company(Model):
    name = models.CharField(
        max_length = 100
        )
    short_name = models.CharField(
        max_length = 100
        )

    class Meta:
        verbose_name_plural = "companhias"
        verbose_name = "companhia"
    
    def __str__(self):
        return self.short_name[:100]


class Rank(Model):
    rank = models.CharField(
        max_length = 100
        )
    short_rank = models.CharField(
        max_length = 100
        )
    order = models.PositiveIntegerField(
        unique = True
        )

    class Meta:
        verbose_name_plural = "postos"
        verbose_name = "posto"
        ordering = ["order"]
    
    def __str__(self):
        return self.short_rank[:100]


class Soldier(Model):
    name = models.CharField(
        max_length = 100
        )
    nim = models.CharField(
        validators=[NIM_REGEX, MinLengthValidator(8)],
        max_length = 8,
        default = '00000000'
        )
    last_name = models.CharField(
        max_length = 100,
        default = 'Milhões'
        )
    promotion_date = models.DateField(
        help_text = "Introduza a data no seguinte formato, por favor: <em>AAAA-MM-DD</em>.",
        default = '2000-01-01'
        )
    course_grade = models.DecimalField(
        max_digits = 4,
        decimal_places = 2,
        help_text = "Introduza nota do último curso de promoção no formato 00,00",
        default = 0.0
        )
    active = models.BooleanField(
        default=True
        )
    rank = models.ForeignKey(
        Rank,
        on_delete=models.CASCADE,
        default = 1
        )
    company = models.ForeignKey(
        Company,
        on_delete=models.CASCADE,
        default = 1
        )
    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
        blank=True,
        null=True
        )

    class Meta:
        verbose_name_plural = "militares"
        verbose_name = "militar"
        ordering = ['rank', '-promotion_date', 'course_grade']
    
    def __str__(self):
        return self.rank.short_rank + ' ' + self.last_name


class Shift(Model):
    name = models.CharField(
        max_length = 100
        )
    daily = models.BooleanField(
        default=True,
        )
    size = models.PositiveIntegerField(
        default = 1,
        )
    start_time = models.TimeField(
        default = time(9,00,00),
        )
    end_time = models.TimeField(
        default = time(9,00,00),
        )
    week = models.BooleanField(
        default=True
        )
    weekend_shift = models.ForeignKey(
        "self",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        )
    hidden = models.BooleanField(
        default=False
        )
    soldiers = models.ManyToManyField(
        Soldier,
        through='SoldierShift'
        )
    
    class Meta:
        verbose_name_plural = "escalas"
        verbose_name = "escala"
    
    def __str__(self):
        return self.name


class SoldierShift(Model):
    soldier = models.ForeignKey(
        Soldier,
        on_delete=models.CASCADE
        )
    shift = models.ForeignKey(
        Shift,
        on_delete=models.CASCADE
        )
    date_last = models.DateField(
        help_text = "Introduza a data no seguinte formato, por favor: <em>AAAA-MM-DD</em>.",
        default = '2000-01-01',
        )
    stand_by = models.BooleanField(
        default=True
        )

    class Meta:
        verbose_name = "militar x escala"
        ordering = ["date_last"]
    
    def __str__(self):
        return self.soldier.rank.short_rank + ' ' + self.soldier.last_name + ' ' + self.shift.name


class Excuse(Model):
    name = models.CharField(
        max_length = 100
        )
    description = models.TextField()
    soldiers = models.ManyToManyField(
        Soldier,
        through='SoldierExcuse'
        )
    
    class Meta:
        verbose_name_plural = "indisponibilidades"
        verbose_name = "indisponibilidade"
    
    def __str__(self):
        return self.name


class SoldierExcuse(Model):
    soldier = models.ForeignKey(
        Soldier,
        on_delete=models.CASCADE
        )
    excuse = models.ForeignKey(
        Excuse,
        on_delete=models.CASCADE,
        )
    gdh_start = models.DateTimeField(
        help_text = "Introduza o grupo data-hora no seguinte formato, por favor: <em>AAAA-MM-DD</em>.",
        #default = '2000-01-01 08:09:10',
        )
    gdh_finish = models.DateTimeField(
        help_text = "Introduza a data no seguinte formato, por favor: <em>AAAA-MM-DD</em>.",
        #default = '2000-01-01 08:09:10',
        )
    description = models.TextField()
    

    class Meta:
        verbose_name = "militar x indisponibilidade"
    
    def __str__(self):
        return self.soldier.rank.short_rank + ' ' + self.soldier.last_name + ' ' + self.excuse.name

##trocas: soldier_id; soldier:id; shift_id; destroca_feita;