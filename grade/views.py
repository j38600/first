# grade/views.py
from django.shortcuts import redirect
from datetime import datetime, timedelta, date
from django.template.response import TemplateResponse
from .models import Soldier, Shift, Holiday, SoldierExcuse, SoldierShift


NUMBER_OF_DAYS_GUESSED = 40

def session_update(request):
    if request.method == 'POST':
        print(request.POST)
        request.session['regimento'] = request.POST['regimento']
    return redirect('home')

def homepage(request):
    if not request.session.get('regimento'):
        request.session['regimento'] = 'RTm'
    return TemplateResponse(request, 'home.html', {})

def soldier_list(request):
    soldiers = Soldier.objects.all()
    return TemplateResponse(request, 'soldiers.html', {
        'all_soldiers_list': soldiers,
    })

def shift_list(request):
    shifts = Shift.objects.all()
    return TemplateResponse(request, 'shifts.html', {
        'all_shifts_list': shifts,
    })

#interna
# Recebe o dia, e ve se é fim de semana ou feriado
def is_week_day(day):
    holiday = Holiday.objects.filter(date=day)
    week_day = True if day.weekday() < 5 and not holiday else False
    return (week_day)

#interna
# Ordenar os militares da escala pelo último svc feito. se teem a mesma
#   folga, a lista dos militares já vem ordenada por posto, data de
#   promoção e de seguida por nota de curso.
def sort_by_last_shift(soldiershift):
    return soldiershift.date_last

#interna
# gera uma lista de soldiershift. Devolve uma lista devidamente ordenada,
#  em que o elemento [0] é o mais folgado.
def sort_shifts(shift):
    lastshift = list()
    soldiers = shift.soldiers.all()
    for soldier in soldiers:
        lastshift += SoldierShift.objects.filter(soldier=soldier, shift=shift)
    lastshift = sorted(lastshift, key=sort_by_last_shift, reverse=True)
    return lastshift

#interna
# gera uma lista de militares a partir de uma lista de soldiershifts.
def list_soldiers(soldiershift, list):
    for soldier in soldiershift:
        if soldier.soldier not in list:
            list.append(soldier.soldier)
    return list

#interna
# gera uma lista de militares a partir de uma lista de soldiershifts.
def fill_nominees(prediction, excuses, lastshift_ordered):
    size = lastshift_ordered[0].shift.size
    week = lastshift_ordered[0].shift.week
    for sameday, day in enumerate(prediction):
        #se for escala fdsemana e dia for fdsemana
        if day['week'] == False == week :
            # inicio aki dentro, porque se puser fora, qd passo
            # a 2a vez, limpo a lista anterior.
            day['nominees'] = list()
            day['excused'] = list()
            # corro o seguinte código SIZE numero de vezes.
            for x in range(size):
                # enqt o mais folgado tiver dispensa para este dia, boto-o para o fim,
                # e adiciono à lista de dispensados para este dia
                while any(excuse.soldier == lastshift_ordered[0].soldier and
                            excuse.gdh_start.date() <= day['day'] <= excuse.gdh_finish.date() for
                            excuse in excuses):
                    day['excused'].append(lastshift_ordered[0].soldier)
                    lastshift_ordered += [lastshift_ordered.pop(0)]
                # o mais folgado sem dispensas é nomeado, o dia do ultimo serviço é atualizado
                # e passo-o para o fim da fila.
                new_soldier = lastshift_ordered[0].soldier
                lastshift_ordered[0].date_last = day['day']
                lastshift_ordered += [lastshift_ordered.pop(0)]
                day['nominees'].append(new_soldier)
        #se for escala semana e dia for semana
        elif day['week'] == True == week:
            day['nominees'] = list()
            day['excused'] = list()
            # o primeiro dia prediction[sameday-1]['week'] vai ser sempre semana,porque o
            # primeiro dia a adivinhar é sempre a seguir a um de semana.
            # o ultimo dia dá indexerror, por isso este try.
            try:
                next_day = prediction[sameday+1]['week']
            except:
                next_day = True # quando ultimo dia não tem indice, assumo que é semana
            
            weekend_nominees = list()
            #se dia antes ou depois não for escala de semana
            # adiciono a uma lista de nomeados
            if not prediction[sameday-1]['week']:
                weekend_nominees = prediction[sameday-1]['nominees']
            elif not next_day:
                weekend_nominees += prediction[sameday+1]['nominees']
            for x in range(size):
                while any(
                    excuse.soldier == lastshift_ordered[0].soldier and
                    excuse.gdh_start.date() <= day['day'] <= excuse.gdh_finish.date() for
                    excuse in excuses
                    ) or any(
                    nominee == lastshift_ordered[0].soldier for
                    nominee in weekend_nominees
                    ):
                    day['excused'].append(lastshift_ordered[0].soldier)
                    lastshift_ordered += [lastshift_ordered.pop(0)]
                new_soldier = lastshift_ordered[0].soldier
                lastshift_ordered[0].date_last = day['day']
                lastshift_ordered += [lastshift_ordered.pop(0)]
                day['nominees'].append(new_soldier)
        #tenho k reordenar, antes de passar para o dia seguinte, porque posso ter
        # nomeado 2 que estavm com folgas de dias diferentes, mas agora é igual,
        # e o anteriormente mais folgado é mais maçarico, então tenho k reordenar.
        lastshift_ordered = sorted(lastshift_ordered, key=sort_by_last_shift)
    return prediction

#funcao para ver escalas que teem versao semana e fdsemana.
def shift_view(request, arg): 
    prediction = list() #lista de dicionário com a seguinte estrutura:
    #   {day: date,
    #   nominees: [list],
    #   excluded: [list],
    #   reserve: [list],
    #   week: bool}
    #lastshift_week = list() #lista de objetos SoldierShift.
    #lastshift_weekend = list() #lista de objetos SoldierShift.
    soldier_list = list() #lista de objetos Soldier,
    queryset_week = Shift.objects.filter(id=arg)
    week_shift = queryset_week.get()
    weekend_shift = week_shift.weekend_shift
    lastshift_week = sort_shifts(week_shift)
    lastshift_weekend = sort_shifts(weekend_shift)
    
    start_date = lastshift_week[0].date_last + timedelta(days=1)
    end_date = lastshift_week[0].date_last + timedelta(days=NUMBER_OF_DAYS_GUESSED)
        
    #aki transformo as listas de soldiershifts, numa de soldiers, unicos!
    soldier_list = list_soldiers(lastshift_week, soldier_list)
    soldier_list = list_soldiers(lastshift_weekend, soldier_list)
    excuses = SoldierExcuse.objects.filter(
        gdh_start__date__gte=start_date,
        soldier__in=soldier_list,
    )
    #aqui já tenho duas listas de militares, lastshift_week, lastshift_weekend
    # [0] é o k fez à menos tempo. Tenho uma lista de desculpas, filtradas por
    # start_date e pelos militares destas duas escalas.
    
    # aki gero o número de dias que vou prever
    while start_date != end_date:
        new_day = {'day': start_date,}
        new_day['week'] = True if is_week_day(start_date) else False
        start_date += timedelta(days=1)
        prediction.append(new_day)
    
    prediction = fill_nominees(prediction, excuses, lastshift_weekend, )
    prediction = fill_nominees(prediction, excuses, lastshift_week)

    #print (prediction)
    return TemplateResponse(request, 'shift.html', {
        'shift': week_shift,
        'previsao': prediction,
        'weekend_shift': weekend_shift,
        'soldiers': lastshift_week,
        'soldiers_weekend': lastshift_weekend,
    })

#def soldier_list(request, arg):
#    return TemplateResponse(request, 'home.html', {})