# grade/admin.py
from django.contrib import admin
from .models import Excuse, Regiment, Soldier, Holiday
from .models import Company, Rank, Shift, SoldierExcuse, SoldierShift


class HolidayAdmin(admin.ModelAdmin):
    list_display = ('name', 'date')
    list_filter = ['date']


class CompanyAdmin(admin.ModelAdmin):
    list_display = ('name', 'short_name')


class SoldierAdmin(admin.ModelAdmin):
    list_display = ('rank', 'nim', 'last_name', 'company')
    list_filter = ('rank', 'company')


class ShiftAdmin(admin.ModelAdmin):
    list_display = ('name', 'size', 'week')
    list_filter = ('week', 'daily')


admin.site.register(Holiday, HolidayAdmin)
admin.site.register(Company, CompanyAdmin)
admin.site.register(Regiment)
admin.site.register(Rank)
admin.site.register(Soldier, SoldierAdmin)
admin.site.register(Shift, ShiftAdmin)
admin.site.register(SoldierShift)
admin.site.register(Excuse)
admin.site.register(SoldierExcuse)